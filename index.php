<?php
header('content-type: application/json; charset=utf-8');

$topics = array(
    
    1 => array(
        [
            'id' => 1,
            'code' => '1',
            'title' => 'ANGGARAN PERBELANJAAN'
        ],
        [
            'id' => 2,
            'code' => '2',
            'title' => 'PENURUNAN KUASA PERBENDAHARAAN KEPADA PEGAWAI PEGAWAL'
        ],
        [
            'id' => 3,
            'code' => '3',
            'title' => 'PENGURUSAN PERBELANJAAN AWAM'
        ],
    ),
    2 => array(
        [
            'id' => 4,
            'code' => '1',
            'title' => 'PUNCA KUASA, PRINSIP DAN DASAR PEROLEHAN KERAJAAN'
        ],
        [
            'id' => 5,
            'code' => '2',
            'title' => 'KAEDAH PEROLEHAN KERAJAAN'
        ],
        [
            'id' => 6,
            'code' => '3',
            'title' => 'PEROLEHAN PERKHIDMATAN PERUNDING'
        ],
    )

);

echo $_REQUEST['callback'] . '('.json_encode($topics[ $_REQUEST['id'] ]).')';