jQuery(document).ready( function($) {

    jQuery('#topic-list-container').hide();

    getFieldList();
    bindFieldItemAction();
    bindBackToField();
});


// get field list from remote server
function getFieldList() {

    jQuery.ajax({
        url: 'http://mof.demosite.my/field.php',
        type: 'GET',
        dataType: 'jsonp',
        success: function( fields ) {

            var html = '';

            fields.forEach( function( field ) {
                html += '<div class="col-xs-6 field-item" data-id="'+field.id+'">';
                html += '<div class="code">';
                html += '<span>'+field.code+'</span>';
                html += '</div>';
                html += '<div class="title">';
                html += field.title;
                html += '</div>';
                html += '</div>';
            });

            jQuery('#field-list-container').html( html );
        }
    });
}

// Bind click event to field
function bindFieldItemAction() {
    jQuery('#field-list-container').on('click', '.field-item', function() {
        getTopicList(jQuery(this).attr('data-id'));
    });
}

// get field list from remote server
function getTopicList( topicID ) {

    jQuery.ajax({
        url: 'http://localhost/index.php',
        type: 'POST',
        data: {
            id: topicID
        },
        dataType: 'jsonp',
        success: function( topics ) {

            var html = '';
            html += '<div class="list-group">';

            topics.forEach( function( topic ) {
                html += '<a href="#" class="list-group-item" data-id="' + topic.id + '">' + topic.title + '</a>';
            });

            html += '</div>';

            jQuery('#topic-list-container .topic-items').html( html )

            jQuery('#field-list-container').slideUp('fast', function() {
                jQuery('#topic-list-container').slideDown('fast');
            });
        }
    });
}

function bindBackToField() {
    jQuery('#btn-back-field').click( function() {
        jQuery('#topic-list-container').slideUp('fast', function() {
            jQuery('#field-list-container').slideDown('fast');
        });
    });
}